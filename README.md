
# Module 0 : Introduction au langage Javascript
- texte : https://wiki.formation-fullstack.fr/minimal/javascript-beginner
- vidéo : m0_c1
- vidéo : m0_c2
- vidéo : m0_c3
- vidéo : m0_c4
- vidéo : m0_c5


# Module 1 : Environnement de développement

## Installation d'un environnement Unix pour Linux et MacOS
- texte : https://wiki.formation-fullstack.fr/cours/online_fullstack/env_linux_mac
- vidéo : https://prismes.univ-toulouse.fr/player.php?code=r2v1Zg6j&width=100%&height=100%

## Installation d'un environnement Unix/WSL pour Windows
- texte : [TODO]
- vidéo : [TODO]

## Installation d'un navigateur pour Linux et MacOS
- texte : https://wiki.formation-fullstack.fr/cours/online_fullstack/browser_linux_mac
- vidéo : https://prismes.univ-toulouse.fr/player.php?code=3t3386GI&width=100%&height=100%

## Installation d'un navigateur pour Windows/WSL
- texte : [TODO]
- vidéo : [TODO]

## Installation de NodeJS pour Linux et MacOSX
- texte : https://wiki.formation-fullstack.fr/cours/online_fullstack/nodejs_linux_mac
- vidéo : https://prismes.univ-toulouse.fr/player.php?code=v2Uf2l2a&width=100%&height=100%

## Installation de NodeJS pour Windows/WSL
- texte : [TODO]
- vidéo : [TODO]

## Usage minimal de VSCode
- texte : https://wiki.formation-fullstack.fr/cours/online_fullstack/vscode
- vidéo : https://prismes.univ-toulouse.fr/player.php?code=q1471P17&width=100%&height=100%

## Usage minimal de Git
- texte : https://wiki.formation-fullstack.fr/cours/online_fullstack/git
- vidéo : [TODO]


# Module 2 : Usage minimal du HTML et du SVG

## Usage minimal du HTML
- texte : https://wiki.formation-fullstack.fr/minimal/html
- vidéo : m2_c1_html_best

## Usage minimal du SVG
- texte : https://wiki.formation-fullstack.fr/minimal/svg
- vidéo : [TODO]


# Module 3 : Usage minimal du CSS
- texte : https://wiki.formation-fullstack.fr/minimal/html
- vidéo : m3_c1_css


# Module 4 : Structure d'une application, requêtes HTTP, websockets, protocole REST

## Structure d'une application web
- texte : https://wiki.formation-fullstack.fr/minimal/web-architecture
- vidéo : [TODO]

## Le protocole HTTP
- texte : https://wiki.formation-fullstack.fr/minimal/http
- vidéo : [TODO]

## Le protocole REST
- texte : https://wiki.formation-fullstack.fr/minimal/rest
- vidéo : [TODO]


# Module 5 : Javascript et NodeJS pour le développement web

## Javascript avancé
- texte : https://wiki.formation-fullstack.fr/minimal/javascript-advanced
- vidéo : [TODO]

## Backend minimal avec json-server
- texte : [TODO]
- vidéo : [TODO]

- déploiement avec Heroku : https://devcenter.heroku.com/articles/deploying-nodejs


# Module 6 : Manipulation directe du DOM

## DOM
- texte : https://wiki.formation-fullstack.fr/minimal/dom
- vidéo : [TODO]

- déploiement avec Surge


# Module 9 : VueJS

## Développement d'un jeu de questions-réponses
- texte : https://wiki.formation-fullstack.fr/cours/online_fullstack/quizz
- vidéo : [TODO]



# Travail à rendre

## Git
Créer un projet git appelé "first-project" sur gitlab.com qui héberge un CV (minimal, fictif) écrit en Markdown

## HTML1
Créer une page web avec la structure d'une image fournie, sans la styler
- responsivité

## HTML2
Créer un formulaire, sans le styler

## CSS1
Styler la page web HTML1 pour qu'elle soit (presque) identique à l'image fournie
- typographie
- couleurs
- dimensions

## CSS2
Styler le formulaire HTML2

# DOM
Créer une application DOM qui visualise le niveau de batterie
